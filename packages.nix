options: let
    combinedOptions = options // {
        overlays = [
            (import ../nixpkgs-mozilla/rust-overlay.nix)
        ];
    };
in {
    master = (import (fetchTarball https://github.com/NixOS/nixpkgs/archive/master.tar.gz) combinedOptions);
    unstable = (import (fetchTarball https://github.com/NixOS/nixpkgs-channels/archive/nixos-unstable.tar.gz) combinedOptions);
    nixpkgs = (import <nixpkgs>) combinedOptions;
    myNixpkgs = (import ../../my-nixpkgs) {};
}
